from django.conf.urls import url
from flipcart_app.views import ProductList, index, product_rating_list #, UserViewSet
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns



urlpatterns = [

    url(r'^$', index),
#     url(r'^products/$', ProductList.as_view()),
    url(r'^products/(?P<id>\d+)/ratings/$', product_rating_list),
]

urlpatterns = format_suffix_patterns(urlpatterns)


router = routers.SimpleRouter()
router.register(r'products', ProductList, 'product_list')

urlpatterns += router.urls