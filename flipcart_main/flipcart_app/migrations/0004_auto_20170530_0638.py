# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-30 06:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flipcart_app', '0003_auto_20170530_0634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='product',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
