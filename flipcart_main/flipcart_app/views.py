# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import serializers, status, viewsets, mixins
from .serializers import ProductsSerializer, RatingSerializer

# Create your views here.
from flipcart_app.models import Products, Rating


def index(request):
    # return HttpResponse('<h1>hello</h1>')
    return render(request, "index.html")

class ProductList(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ProductsSerializer
    queryset = Products.objects.all()

    # def list(self, request):
    #     products = Products.objects.all()
    #     serializer = ProductsSerializer(products, many=True)
    #     return Response(serializer.data[0])

    def post(self):
        pass

# class UserViewSet (viewsets.ViewSet):
#     def list(self, request):
#         queryset = User.objects.all()
#         serializer = ProductsSerializer(queryset, many=True)
#         return Response(serializer.data)
#
#     def retreive(self, request, pk=None):
#         queryset = User.objects.all()
#         user = get_object_or_404(queryset, pk=pk)
#         serializer = ProductsSerializer(user)
#         return Response(serializer.data)



# def product_list(request):
#     if request.method == "POST":
#         name=request.POST.get("name")
#         cost=request.POST.get("cost")
#         type=request.POST.get("type")
#         brand=request.POST.get("brand")
#         image=request.POST.get("image")
#         Products.objects.create(name=name, cost=cost, type=type, brand=brand, image=image)
#     queryset = Products.objects.all()
#     context = {
#         "prod_list":queryset
#     }
    # s = PostSerializer(queryset, many=True)
    # dicts = []
    # for product in queryset:
    #     dicts.append({
    #         "name": product.name,
    #         "cost": product.cost,
    #         "type": product.type,
    #         "brand":product.brand,
    #     })
    #
    # d = json.dumps(dicts)
    # return HttpResponse(d)

#     return render(request, "product_list.html", context)
def product_rating_list(request, id=None):
    instance = get_object_or_404(Rating, id=id)
    context = {
        "rating":instance
    }
    return render(request, "product_rating.html", context)