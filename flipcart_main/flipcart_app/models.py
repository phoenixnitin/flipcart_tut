# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Products(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    cost = models.FloatField(blank=True, null=True)
    type = models.CharField(max_length=50, blank=True, null=True)
    brand = models.CharField(max_length=50, blank=True, null=True)
    image = models.ImageField(blank=True, null=True)

    # def __str__(self):
    #     return '\nname = ' + self.name + ', type = ' + self.type + ', brand = ' + self.brand + '\n'

class Rating(models.Model):
    rating_choices = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    product = models.CharField(max_length=50, blank=True)
    user = models.CharField(max_length=50, blank=True)
    integer = models.IntegerField(blank=True, choices=rating_choices, default=1)

    def __str__(self):
        return '\nproduct = ' + self.product + ', user = ' + self.user + ', integer = ' +  str(self.integer) + '\n'